import express from 'express';

const app = express();

app.get('/will', (req, res) => {
  res.send(`{ "response" : "Hi There!"}`);
});

app.get('/ready', (req, res) => {
  res.send(`{ "response" : "It works!"}`);
});

app.listen( process.env.PORT || 3000, () => {
  console.log("App is ready on port 3000");
});

export { app };
