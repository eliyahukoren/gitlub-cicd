import request from "supertest";
import { app } from '../index';

it("GET /will", (done) => {
  request(app).get("/will").expect(`{ "response" : "Hi There!"}`, done);
});
